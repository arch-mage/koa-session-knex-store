# koa-session-knex-store

`koa-session-knex-store` connect-session-knex is an
[koa-session][koa-session-url] store backed by PostgreSQL, MySQL, MariaDB,
MSSQL, Oracle or SQLite3, via the [knex.js][knex-url] library. This
is a modified version of [connect-session-knex][connect-session-knex-url].

## installation

```sh
$ npm install koa-session-knex-store
```

or

```sh
$ yarn add koa-session-knex-store
```

## usage

```javascript
const koa = require('koa')
const session = require('koa-session')
const knex = require('knex')({
  client: 'sqlite3',
  connection: {
    filename: ':memory:',
  },
  useNullAsDefault: true,
})
const store = require('koa-session-knex-store')(knex, {
  createtable: true
})

const app = new koa()

app.keys = ['secret key']
app.use(session({ store }, app))

app.use((ctx, next) => {
  switch (ctx.path) {
    case '/':
      let n = ctx.session.count || 0
      ctx.session.count = ++n
      ctx.body = n
      break
    case '/destroy':
      ctx.session = null
      ctx.body = 'destroyed'
      break
    default:
      return next()
  }
})

app.listen(process.env.PORT || 3000)
```

## Options

 - `tablename='sessions'` Tablename to use. Defaults to `'session'`.
 - `sidfieldname='sid'` Field name in table to use for storing session ids. Defaults to `'sid'`.
 - `createtable=false` if the table for sessions should be created automatically or not. Defaults to `false`.

## schema

### PostgreSQL or SQLite

#### Table Name "session"  
| Column  |           Type           | Modifiers | Storage  | 
|---------|:------------------------:|:---------:|:--------:|
| sid     | character varying(255)   | not null  | extended |
| sess    | json                     | not null  | extended |
| expired | timestamp with time zone | not null  | plain    |  

### Indexes:
```  
    "sessions_pkey" PRIMARY KEY, btree (sid)  
    "sessions_expired_index" btree (expired)
```

## credits

Original version by [llambda](https://github.com/llambda)

[knex-url]: http://knexjs.org/
[koa-session-url]: https://github.com/koajs/session
[connect-session-knex-url]: https://github.com/llambda/connect-session-knex
