import * as Knex from 'knex'
import { Session, stores as Store } from 'koa-session'

const ONE_DAY = 86400000

/**
 * @private
 * Return datastore appropriate string of the current time
 *
 * @param knex Knex instance
 * @param aDate optional date
 * @return {string | Date}
 */
function dateAsISO(knex: Knex, aDate?: number): string | Date {
  var date
  if (aDate != null) {
    date = new Date(aDate)
  } else {
    date = new Date()
  }
  if (isOracle(knex)) {
    return date
  }
  return isMySQL(knex) || isMSSQL(knex)
    ? date
        .toISOString()
        .slice(0, 19)
        .replace('T', ' ')
    : date.toISOString()
}

/**
 * @private
 * Return dialect-aware type name for timestamp
 *
 * @return {string} type name for timestamp
 */
function timestampTypeName(knex: Knex): string {
  return isMySQL(knex) || isMSSQL(knex)
    ? 'DATETIME'
    : isPostgres(knex)
      ? 'timestamp with time zone'
      : 'timestamp'
}

/**
 * @private
 * Return condition for filtering by expiration
 *
 * @return {string} expired sql condition string
 */
function expiredCondition(knex: Knex): string {
  var condition = 'CAST(? as ' + timestampTypeName(knex) + ') <= expired'
  if (isSqlite3(knex)) {
    // sqlite3 date condition is a special case.
    condition = 'datetime(?) <= datetime(expired)'
  } else if (isOracle(knex)) {
    condition = 'CAST(? as ' + timestampTypeName(knex) + ') <= "expired"'
  }
  return condition
}

/**
 * @private
 * Returns true if the specified knex instance is using sqlite3.
 *
 * @return {boolean}
 */
function isSqlite3(knex: Knex): boolean {
  return knex.client.dialect === 'sqlite3'
}

/**
 * @private
 * Returns true if the specified knex instance is using mysql.
 *
 * @return {boolean}
 */
function isMySQL(knex: Knex): boolean {
  return ['mysql', 'mariasql', 'mariadb'].indexOf(knex.client.dialect) > -1
}

/**
 * @private
 * Returns true if the specified knex instance is using mssql.
 *
 * @return {boolean}
 */
function isMSSQL(knex: Knex): boolean {
  return ['mssql'].indexOf(knex.client.dialect) > -1
}

/**
 * @private
 * Returns true if the specified knex instance is using postgresql.
 *
 * @return {boolean}
 */
function isPostgres(knex: Knex): boolean {
  return ['postgresql'].indexOf(knex.client.dialect) > -1
}

/**
 * @private
 * Returns true if the specified knex instance is using oracle.
 *
 * @return {boolean}
 */
function isOracle(knex: Knex): boolean {
  return ['oracle', 'oracledb'].indexOf(knex.client.dialect) > -1
}

/**
 * @private
 * Returns PostgreSQL fast upsert query.
 *
 * @return {string}
 */
function getPostgresFastQuery(tablename: string, sidfieldname: string): string {
  return (
    'with new_values (' +
    sidfieldname +
    ', expired, sess) as (' +
    '  values (?, ?::timestamp with time zone, ?::json)' +
    '), ' +
    'upsert as ' +
    '( ' +
    '  update ' +
    tablename +
    ' cs set ' +
    '    ' +
    sidfieldname +
    ' = nv.' +
    sidfieldname +
    ', ' +
    '    expired = nv.expired, ' +
    '    sess = nv.sess ' +
    '  from new_values nv ' +
    '  where cs.' +
    sidfieldname +
    ' = nv.' +
    sidfieldname +
    ' ' +
    '  returning cs.* ' +
    ')' +
    'insert into ' +
    tablename +
    ' (' +
    sidfieldname +
    ', expired, sess) ' +
    'select ' +
    sidfieldname +
    ', expired, sess ' +
    'from new_values ' +
    'where not exists (select 1 from upsert up where up.' +
    sidfieldname +
    ' = new_values.' +
    sidfieldname +
    ')'
  )
}

/**
 * @private
 * Returns SQLite fast upsert query.
 *
 * @return {string}
 */
function getSqliteFastQuery(tablename: string, sidfieldname: string): string {
  return (
    'insert or replace into ' +
    tablename +
    ' (' +
    sidfieldname +
    ', expired, sess) values (?, ?, ?);'
  )
}

/**
 * @private
 * Returns MySQL fast upsert query.
 *
 * @return {string}
 */
function getMysqlFastQuery(tablename: string, sidfieldname: string): string {
  return (
    'insert into ' +
    tablename +
    ' (' +
    sidfieldname +
    ', expired, sess) values (?, ?, ?) on duplicate key update expired=values(expired), sess=values(sess);'
  )
}

/**
 * @private
 * Returns MSSQL fast upsert query.
 *
 * @return {string}
 */
function getMssqlFastQuery(tablename: string, sidfieldname: string): string {
  return (
    'merge ' +
    tablename +
    ' as T ' +
    'using (values (?, ?, ?)) as S (' +
    sidfieldname +
    ', expired, sess) ' +
    'on (T.' +
    sidfieldname +
    ' = S.' +
    sidfieldname +
    ') ' +
    'when matched then ' +
    'update set expired = S.expired, sess = S.sess ' +
    'when not matched by target then ' +
    'insert (' +
    sidfieldname +
    ', expired, sess) values (S.' +
    sidfieldname +
    ', S.expired, S.sess) ' +
    'output inserted.*;'
  )
}

/**
 * @private
 * Remove expired sessions from database.
 *
 * @param {Object} store
 * @param {number} interval
 */
function dbCleanup(store: any, interval: number) {
  return store.ready
    .then(function() {
      var condition =
        'expired < CAST(? as ' + timestampTypeName(store.knex) + ')'
      if (isSqlite3(store.knex)) {
        // sqlite3 date condition is a special case.
        condition = 'datetime(expired) < datetime(?)'
      } else if (isOracle(store.knex)) {
        condition =
          '"expired" < CAST(? as ' + timestampTypeName(store.knex) + ')'
      }
      return store
        .knex(store.tablename)
        .del()
        .whereRaw(condition, dateAsISO(store.knex))
    })
    .finally(function() {
      setTimeout(() => dbCleanup(store, interval), interval).unref()
      // setTimeout(dbCleanup, interval, store, interval).unref()
    })
}

const DEFAULT_OPTIONS = {
  tablename: 'session',
  sidfieldname: 'sid',
  createtable: false,
}

function createStore(knex: Knex, options: createStore.Options = {}): Store {
  const tablename = options.tablename || DEFAULT_OPTIONS.tablename
  const sidfieldname = options.sidfieldname || DEFAULT_OPTIONS.sidfieldname
  const createtable = options.createtable === true

  const ready = knex.schema.hasTable(tablename).then(async exists => {
    if (exists || !createtable) return exists
    await createTable(knex, tablename, sidfieldname)
    return exists
  })

  return {
    async get(sid: string, maxAge: number) {
      await ready
      const condition = expiredCondition(knex)
      const result = await knex
        .select('sess')
        .from(tablename)
        .where(sidfieldname, '=', sid)
        .whereRaw(condition, dateAsISO(knex))
        .first()
      if (!result) return
      const { sess } = result
      return typeof sess === 'string' ? JSON.parse(sess) : sess
    },
    async set(sid: string, session: Session, maxAge: number) {
      await ready
      const now = new Date().getTime()
      const expired = maxAge ? now + maxAge : now + ONE_DAY
      const sess = JSON.stringify(session)
      const dbDate = dateAsISO(knex, expired)

      const bindings = [sid, dbDate, sess]

      if (isSqlite3(knex)) {
        // sqlite optimized query
        return knex.raw(getSqliteFastQuery(tablename, sidfieldname), bindings)
      } else if (isPostgres(knex) && parseFloat(knex.client.version) >= 9.2) {
        // postgresql optimized query
        return knex.raw(getPostgresFastQuery(tablename, sidfieldname), bindings)
      } else if (isMySQL(knex)) {
        // mysql/mariaDB optimized query
        return knex.raw(getMysqlFastQuery(tablename, sidfieldname), bindings)
      } else if (isMSSQL(knex)) {
        // mssql optimized query
        return knex.raw(getMssqlFastQuery(tablename, sidfieldname), bindings)
      } else {
        return knex.transaction(async trx => {
          const foundKeys = await trx
            .select('*')
            .forUpdate()
            .from(tablename)
            .where(sidfieldname, '=', sid)
            .first()

          if (foundKeys) {
            await trx(tablename)
              .where(sidfieldname, '=', sid)
              .update({ expired: dbDate, sess })
          } else {
            await trx.from(tablename).insert({
              sid,
              sess,
              expired: dbDate,
            })
          }
        })
      }
    },
    async destroy(sid: string) {
      await ready
      await knex(tablename)
        .where(sidfieldname, '=', sid)
        .del()
    },
  }
}

function createMigration(
  tablename = DEFAULT_OPTIONS.tablename,
  sidfieldname = DEFAULT_OPTIONS.sidfieldname
): createStore.Migration {
  return {
    async up(knex: Knex) {
      return createTable(knex, tablename, sidfieldname)
    },
    async down(knex: Knex) {
      return dropTable(knex, tablename)
    },
  }
}

function createTable(
  knex: Knex,
  tablename = DEFAULT_OPTIONS.tablename,
  sidfieldname = DEFAULT_OPTIONS.sidfieldname
): Knex.SchemaBuilder {
  return knex.schema.createTable(tablename, table => {
    table.string(sidfieldname).primary()
    table.json('sess').notNullable()
    table
      .timestamp('expired')
      .notNullable()
      .index()
  })
}

function dropTable(
  knex: Knex,
  tablename = DEFAULT_OPTIONS.tablename
): Knex.SchemaBuilder {
  return knex.schema.dropTable(tablename)
}

declare namespace createStore {
  export interface Options {
    tablename?: string
    sidfieldname?: string
    createtable?: boolean
  }
  export interface Migration {
    up: (knex: Knex) => Promise<any>
    down: (knex: Knex) => Promise<any>
  }
  export function createStore(knex: Knex, options?: Options): Store
  export function createMigration(
    tablename?: string,
    sidfieldname?: string
  ): Migration
  export function createTable(
    knex: Knex,
    tablename?: string,
    sidfieldname?: string
  ): Knex.SchemaBuilder
  export function dropTable(knex: Knex): Knex.SchemaBuilder
}

createStore.createStore = createStore
createStore.createMigration = createMigration
createStore.createTable = createTable
createStore.dropTable = dropTable

export = createStore
