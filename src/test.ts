import { createStore, createMigration } from './index'
import * as Knex from 'knex'

const configs = [
  {
    client: 'sqlite3',
    connection: { filename: ':memory:' },
    useNullAsDefault: true,
  },
]

configs.forEach(config => {
  const knex = Knex(config)
  const store = createStore(knex)
  const migration = createMigration()

  describe(config.client, () => {
    beforeAll(async () => {
      await migration.up(knex)
    })

    afterAll(async () => {
      await migration.down(knex)
      await knex.destroy()
    })

    test('set', async () => {
      await store.set('0001', { value: '0001' })
      await store.set('0002', { value: '0002' })
    })

    test('get', async () => {
      const sess = await store.get('0001')
      expect(sess).toEqual({ value: '0001' })
    })

    test('get set session', async () => {
      const sess = await store.get('0003')
      expect(sess).toBeUndefined()
    })

    test('destroy', async () => {
      await store.destroy('0001')
      const sess = await store.get('0001')
      expect(sess).toBeUndefined()
    })
  })
})
