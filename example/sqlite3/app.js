const koa = require('koa')
const session = require('koa-session')
const knex = require('knex')({
  client: 'sqlite3',
  connection: {
    filename: ':memory:',
  },
  useNullAsDefault: true,
})
const store = require('../..')(knex, { createtable: true })

const app = new koa()

app.keys = ['secret key']
app.use(session({ store }, app))

app.use((ctx, next) => {
  switch (ctx.path) {
    case '/':
      let n = ctx.session.count || 0
      ctx.session.count = ++n
      ctx.body = n
      break
    case '/destroy':
      ctx.session = null
      ctx.body = 'destroyed'
      break
    default:
      return next()
  }
})

app.listen(process.env.PORT || 3000)
